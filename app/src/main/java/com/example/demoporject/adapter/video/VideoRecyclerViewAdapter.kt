package com.example.demoporject.adapter.video

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dueeeke.videocontroller.component.PrepareView
import com.example.demoporject.R
import com.example.demoporject.model.Video.VideoBean

class VideoRecyclerViewAdapter(
        private val videoBeanList: MutableList<VideoBean> = mutableListOf()
) : RecyclerView.Adapter<VideoRecyclerViewAdapter.VideoHolder>() {

    interface OnItemChildClickListener {
        fun onItemChildClick(position: Int)
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int)
    }

    private var itemChildClickListener: OnItemChildClickListener? = null
    private var itemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_video, parent, false)
        return VideoHolder(itemView)
    }

    override fun onBindViewHolder(holder: VideoHolder, position: Int) {
        val (_, title, thumb, _) = videoBeanList[position]
        Glide.with(holder.imageView.context)
                .load(thumb)
                .placeholder(android.R.color.darker_gray)
                .into(holder.imageView)
        holder.textView.text = title
        holder.mPosition = position
    }

    override fun getItemCount(): Int {
        return videoBeanList.size
    }

    fun addData(videoList: List<VideoBean>) {
        val size = videoBeanList.size
        videoBeanList.addAll(videoList)
        notifyItemRangeChanged(size, videoBeanList.size)
    }

    inner class VideoHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var mPosition = 0
        var frameLayout: FrameLayout = itemView.findViewById(R.id.frame_layout_player_container)
        var textView: TextView = itemView.findViewById(R.id.text_view_title)
        var imageView: ImageView
        var prepareView: PrepareView = itemView.findViewById(R.id.prepare_view)

        init {
            imageView = prepareView.findViewById(R.id.thumb)
            if (itemChildClickListener != null) frameLayout.setOnClickListener(this)
            if (itemClickListener != null) itemView.setOnClickListener(this)
            itemView.tag = this
        }

        override fun onClick(v: View) {
            if (v.id == R.id.frame_layout_player_container) {
                if (itemChildClickListener != null) {
                    itemChildClickListener!!.onItemChildClick(mPosition)
                }
            } else {
                if (itemClickListener != null) {
                    itemClickListener!!.onItemClick(mPosition)
                }
            }
        }
    }

    fun setOnItemChildClickListener(onItemChildClickListener: OnItemChildClickListener?) {
        itemChildClickListener = onItemChildClickListener
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
        itemClickListener = onItemClickListener
    }
}