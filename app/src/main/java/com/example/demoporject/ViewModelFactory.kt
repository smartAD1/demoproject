package com.example.demoporject

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.demoporject.repository.UserRepo.UserRepository
import com.example.demoporject.repository.ViedoRepo.VideoRepository
import com.example.demoporject.viewModel.PictureViewModel
import com.example.demoporject.viewModel.UserViewModel
import com.example.demoporject.viewModel.VideoViewModel

class ViewModelFactory(val context: Context) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T =
        with(modelClass) {
            when {
                isAssignableFrom(UserViewModel::class.java) -> UserViewModel(
                    UserRepository()
                )
                isAssignableFrom(VideoViewModel::class.java) -> VideoViewModel(VideoRepository())
                isAssignableFrom(PictureViewModel::class.java) -> PictureViewModel(context)
                else -> throw IllegalArgumentException(
                    "Unknown ViewModel class: ${modelClass.name}")
            }

        }as T
}