package com.example.demoporject.repository.UserRepo

import com.example.demoporject.model.user.SearchUserResponse
import com.example.demoporject.model.user.UserResponse
import io.reactivex.Single
import retrofit2.Response

interface UserRepositoryContact {

    fun addUserData(userID: String, passWord: String,
                    memberName: String, mail: String, quota: String): Single<Response<UserResponse>>

    fun deleteUserData(userID: String): Single<Response<UserResponse>>

    fun searchUserData(userID: String): Single<Response<SearchUserResponse>>

    fun modifyUserData(userID: String, passWord: String,
                       memberName: String, mail: String, quota: String): Single<Response<UserResponse>>

}