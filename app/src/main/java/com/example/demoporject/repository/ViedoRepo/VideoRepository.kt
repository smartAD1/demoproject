package com.example.demoporject.repository.ViedoRepo

import com.example.demoporject.apiUtils.RetrofitManager
import com.example.demoporject.model.Video.GetVideoListResponse
import io.reactivex.Single
import retrofit2.Response

class VideoRepository: VideoRepositoryContact {
    private val manager = RetrofitManager.getVideoRetrofitManager()
    override fun getVideoList(pageNo: String, pageSize: String) =
        manager.getVideoList(pageNo = pageNo,pageSize = pageSize)

    override fun getVideoData(videoId: String, memberId: String): Single<Response<GetVideoListResponse>> =
        manager.getVideoData(videoId = videoId,memberId = memberId)

    override fun getVideoStatistics(): Single<Response<GetVideoListResponse>> =
        manager.getVideoStatistics("")

}