package com.example.demoporject.repository.ViedoRepo

import com.example.demoporject.model.Video.GetVideoListResponse
import io.reactivex.Single
import retrofit2.Response

interface VideoRepositoryContact {

    fun getVideoList(pageNo: String, pageSize: String): Single<Response<GetVideoListResponse>>

    fun getVideoData(videoId: String, memberId: String): Single<Response<GetVideoListResponse>>

    fun getVideoStatistics(): Single<Response<GetVideoListResponse>>
}