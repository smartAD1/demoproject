package com.example.demoporject.repository.UserRepo

import com.example.demoporject.apiUtils.RetrofitManager

class UserRepository :
    UserRepositoryContact {
    private val retrofitManager = RetrofitManager.getUserRetrofitManager()

    override fun addUserData(
        userID: String, passWord: String, memberName: String,
        mail: String, quota: String) =
            retrofitManager.addUserData(userId = userID, password = passWord
                    , memberName = memberName, mail = mail, quota = quota)

    override fun modifyUserData(
        userID: String, passWord: String, memberName: String,
        mail: String, quota: String) =
            retrofitManager.modifyUserData(
                userid = userID, password = passWord
                    , member_name = memberName, mail = mail, quota = quota)

    override fun deleteUserData(userID: String) =
        retrofitManager.deleteUserData(userid = userID)

    override fun searchUserData(userID: String) = retrofitManager.searchUserData(
        userid = userID)

}