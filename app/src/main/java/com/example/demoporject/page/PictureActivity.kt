package com.example.demoporject.page

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.demoporject.R
import com.example.demoporject.ViewModelFactory
import com.example.demoporject.utils.glide.GlideApp
import com.example.demoporject.viewModel.PictureViewModel
import kotlinx.android.synthetic.main.activity_picture.*

class PictureActivity : AppCompatActivity() {
    var selectImg = "404"
    private val viewModel: PictureViewModel by lazy {
        ViewModelProvider(this, ViewModelFactory(this)).get(PictureViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picture)
        setClickEvent()
        initViewModel()
    }

    private fun setClickEvent() {
        radio_group.setOnCheckedChangeListener { group, checkedId ->
            selectImg = when (checkedId) {
                R.id.background -> "background"
                R.id.movie -> "movie"
                else -> "404"
            }
        }
        button_generate_base_64.setOnClickListener {
            viewModel.generateBase64(selectImg)
            tv_base_text.text = viewModel.base64String
        }
        button_load_base_64.setOnClickListener {
            val string =  viewModel.base64String
            GlideApp.with(this)
                .load(string)
                .into(image_view)
        }
    }

    private fun initViewModel() {
        viewModel.run {
            base64PictureString.observe(this@PictureActivity, Observer {
                tv_base_text.text = ""
                tv_base_text.text = it
            })
        }
    }
}