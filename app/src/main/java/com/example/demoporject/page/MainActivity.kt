package com.example.demoporject.page

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.demoporject.R
import com.example.demoporject.ViewModelFactory
import com.example.demoporject.viewModel.UserDataStatus
import com.example.demoporject.viewModel.UserViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val viewModel: UserViewModel by lazy {
        ViewModelProvider(this, ViewModelFactory(this)).get(UserViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setClickEvent()
        initViewModel()
    }
    private fun setClickEvent() {
        btn_add_user.setOnClickListener {
            viewModel.addUserData(userID = "${et_user_account.text}",userPwd = "${et_user_pwd.text}",
            memberName = "${et_user_name.text}",mail = "${et_user_email.text}",quoData = "${et_user_quota.text}")
        }
        btn_search_user.setOnClickListener {
            viewModel.searchUserData(userID = "${et_user_account.text}")
        }
        btn_modify_user.setOnClickListener {
            viewModel.modifyUserData(userID = "${et_user_account.text}",userPwd = "${et_user_pwd.text}",
                memberName = "${et_user_name.text}",mail = "${et_user_email.text}",quoData = "${et_user_quota.text}")
        }
        btn_delete_user.setOnClickListener {
            viewModel.deleteUserData(userID = "${et_user_account.text}")
        }
        btn_goto_video.setOnClickListener {
            startActivity(Intent(this,VideoActivity::class.java))
        }
        btn_goto_pic_decode.setOnClickListener {
            startActivity(Intent(this,PictureActivity::class.java))
        }
    }
    private fun initViewModel() =
        viewModel.run {
            userData.observe(this@MainActivity, Observer {
                when(it) {
                    is UserDataStatus.AddStatus -> showToast("${it.data}")
                    is UserDataStatus.UpDateStatus -> showToast("${it.data}")
                    is UserDataStatus.DeleteStatus -> showToast("${it.data}")
                }
            })
            userSearchData.observe(this@MainActivity, Observer {
                showToast("$it")
            })
        }
    fun showToast(data: String) {
        Toast.makeText(this,data,Toast.LENGTH_SHORT).show()
    }
}