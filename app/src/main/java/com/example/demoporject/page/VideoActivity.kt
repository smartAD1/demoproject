package com.example.demoporject.page

import android.content.pm.ActivityInfo
import android.graphics.Rect
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dueeeke.videocontroller.StandardVideoController
import com.dueeeke.videocontroller.component.*
import com.dueeeke.videoplayer.player.AbstractPlayer
import com.dueeeke.videoplayer.player.VideoView
import com.dueeeke.videoplayer.player.VideoViewManager
import com.example.demoporject.R
import com.example.demoporject.ViewModelFactory
import com.example.demoporject.adapter.video.VideoRecyclerViewAdapter
import com.example.demoporject.model.Video.VideoBean
import com.example.demoporject.viewModel.VideoViewModel
import kotlinx.android.synthetic.main.activity_video.*

class VideoActivity : AppCompatActivity(), VideoRecyclerViewAdapter.OnItemChildClickListener {

    private val viewModel by lazy {
        ViewModelProvider(this, ViewModelFactory(this)).get(VideoViewModel::class.java)
    }
    private val videoView: VideoView<AbstractPlayer> by lazy {
        VideoView<AbstractPlayer>(this)
    }
    private val adapter: VideoRecyclerViewAdapter by lazy { VideoRecyclerViewAdapter() }
    private val layoutManager: LinearLayoutManager by lazy { LinearLayoutManager(this) }
    private val videoController: StandardVideoController by lazy { StandardVideoController(this) }
    private val errorView: ErrorView by lazy { ErrorView(this) }
    private val completeView: CompleteView by lazy { CompleteView(this) }
    private val titleView: TitleView by lazy { TitleView(this) }
    private val videoViewManager: VideoViewManager by lazy { VideoViewManager.instance() }
    private var currentPosition = -1
    private var lastPosition = currentPosition
    private val videoBeanList = mutableListOf<VideoBean>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)
        val pageNo = 1.toString()
        val pageSize = 5.toString()
        viewModel.getVideoList(pageNo, pageSize)
        initVideoView()
        initRecyclerView()
        initViewModel()
    }

    private fun initViewModel() =
        viewModel.run {
            videoListLiveData.observe(this@VideoActivity, Observer {
                it.data.forEach {
                    videoBeanList.add(VideoBean(it.id, it.title, it.thumb, it.link))
                }
                adapter.addData(videoBeanList)
                rv_video.post { startPlay(0) }
            })
        }


    private fun initVideoView() {
        videoView.setOnStateChangeListener(object : VideoView.SimpleOnStateChangeListener() {
            override fun onPlayStateChanged(playState: Int) {
                if (playState == VideoView.STATE_IDLE) {
                    removeViewFormParent(videoView)
                    lastPosition = currentPosition
                    currentPosition = -1
                }
            }
        })
        videoController.apply {
            addControlComponent(errorView)
            addControlComponent(completeView)
            addControlComponent(titleView)
            addControlComponent(VodControlView(this@VideoActivity))
            addControlComponent(GestureView(this@VideoActivity))
            setEnableOrientation(true)
        }.run {
            videoView.setVideoController(this)
        }
    }

    private fun initRecyclerView() {
        rv_video.let {
            it.layoutManager = layoutManager
            adapter.setOnItemChildClickListener(this)
            it.adapter = adapter
            it.addOnChildAttachStateChangeListener(object :
                RecyclerView.OnChildAttachStateChangeListener {
                override fun onChildViewAttachedToWindow(view: View) = Unit
                override fun onChildViewDetachedFromWindow(view: View) {
                    val playerContainer =
                        view.findViewById<FrameLayout>(R.id.frame_layout_player_container)
                    val v = playerContainer.getChildAt(0)
                    if (v != null && v === videoView && !videoView.isFullScreen) releaseVideoView()
                }
            })
            it.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (newState == RecyclerView.SCROLL_STATE_IDLE) autoPlayVideo(recyclerView)
                }
            })
        }
    }

    private fun autoPlayVideo(view: RecyclerView?) {
        if (view == null) return
        val count = view.childCount
        for (i in 0 until count) {
            val itemView = view.getChildAt(i) ?: continue
            val holder: VideoRecyclerViewAdapter.VideoHolder =
                itemView.tag as VideoRecyclerViewAdapter.VideoHolder
            val rect = Rect()
            holder.frameLayout.getLocalVisibleRect(rect)
            val height: Int = holder.frameLayout.height
            if (rect.top == 0 && rect.bottom == height) {
                startPlay(holder.mPosition)
                break
            }
        }
    }

    private fun startPlay(position: Int) {
        if (currentPosition == position) return
        if (currentPosition != -1) releaseVideoView()
        val videoBean = videoBeanList[position]
        videoView.setUrl(videoBean.url)
        titleView.setTitle(videoBean.title)
        val itemView = layoutManager.findViewByPosition(position) ?: return
        val viewHolder = itemView.tag as VideoRecyclerViewAdapter.VideoHolder
        videoController.addControlComponent(viewHolder.prepareView, true)
        removeViewFormParent(videoView)
        viewHolder.frameLayout.addView(videoView, 0)
        videoViewManager.add(videoView, "list")
        videoView.start()
        currentPosition = position
        val videoId = videoBeanList[position].id
        // 因為只是測試階段 也沒有登入的api 就先假設memberId為34的會員已登入
        val memberId = 34.toString()
        viewModel.getVideoData(videoId, memberId)
    }

    fun removeViewFormParent(view: View?) {
        if (view == null) return
        val parent = view.parent
        if (parent is FrameLayout) parent.removeView(view)
    }

    private fun releaseVideoView() {
        videoView.release()
        if (videoView.isFullScreen) videoView.stopFullScreen()
        if (this.requestedOrientation != ActivityInfo.SCREEN_ORIENTATION_PORTRAIT) {
            this.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
        currentPosition = -1
    }

    override fun onResume() {
        super.onResume()
        if (lastPosition == -1) return
        startPlay(lastPosition)
    }

    override fun onPause() {
        releaseVideoView()
        super.onPause()
    }

    override fun onBackPressed() {
        if (videoViewManager.onBackPress("list")) return
        super.onBackPressed()
    }

    override fun onItemChildClick(position: Int) {
        startPlay(position)
    }
}