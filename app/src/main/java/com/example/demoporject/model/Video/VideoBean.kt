package com.example.demoporject.model.Video

data class VideoBean(
    val id: String,
    val title: String,
    val thumb: String,
    val url: String
)