package com.example.demoporject.model.Video

import com.google.gson.annotations.SerializedName

data class VideoResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val data: VideoResponseData,
    @SerializedName("mag")
    val mag: String
)

data class VideoResponseData(
    @SerializedName("create_time")
    val createTime: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("link")
    val link: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("update_time")
    val updateTime: String,
    @SerializedName("video_id")
    val videoId: String,
    @SerializedName("total")
    val total: String
)