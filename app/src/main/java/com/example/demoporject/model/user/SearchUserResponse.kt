package com.example.demoporject.model.user

import com.google.gson.annotations.SerializedName

data class SearchUserResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val data: SearchUserResponseData,
    @SerializedName("msg")
    val msg: String
)

data class SearchUserResponseData(
    @SerializedName("create_time")
    val createTime: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("mail")
    val mail: String,
    @SerializedName("member_name")
    val memberName: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("quota")
    val quota: String,
    @SerializedName("update_time")
    val updateTime: Any,
    @SerializedName("userid")
    val userid: String
)