package com.example.demoporject.model.user

import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("code")
    var code: Int = 0,
    @SerializedName("data")
    var data: String = "",
    @SerializedName("message")
    var message: String = "",
    @SerializedName("msg")
    var msg: String = ""
)