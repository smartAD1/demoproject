package com.example.demoporject.model.Video

import com.google.gson.annotations.SerializedName

data class GetVideoListResponse(
    @SerializedName("code")
    val code: Int,
    @SerializedName("data")
    val data: List<GetVideoListResponseData>
)

data class GetVideoListResponseData(
    @SerializedName("create_time")
    val createTime: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("id")
    val id: String,
    @SerializedName("thumb")
    val thumb: String,
    @SerializedName("link")
    val link: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("update_time")
    val updateTime: Any
)