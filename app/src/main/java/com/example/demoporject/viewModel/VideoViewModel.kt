package com.example.demoporject.viewModel

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.demoporject.model.Video.GetVideoListResponse
import com.example.demoporject.model.Video.VideoResponse
import com.example.demoporject.repository.ViedoRepo.VideoRepository
import com.example.demoporject.utils.addDisposable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.observers.DisposableObserver
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers

class VideoViewModel(private val repository: VideoRepository) : BaseViewModel() {
    val videoListLiveData :LiveData<GetVideoListResponse> get() = _videoListLiveData
    private val _videoListLiveData: MutableLiveData<GetVideoListResponse> = MutableLiveData()
    @SuppressLint("CheckResult")
    fun getVideoList(
        pageNo: String,
        pageSize: String
    ) {
        repository.getVideoList(pageNo, pageSize)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onSuccess = {
                if (it.isSuccessful) {
                    it.body()?.let {
                        _videoListLiveData.value = it
                    }
                }
            }, onError = {
                Log.d("myViewData","GG")
            }).addDisposable(compositeDisposable)
//            .subscribeWith(object : DisposableObserver<GetVideoListResponse>() {
//                override fun onNext(response: GetVideoListResponse) {
//                    videoListLiveData.value = response
//                }
//
//                override fun onError(throwable: Throwable) {
//                }
//
//                override fun onComplete() {
//                }
//            })
    }

    @SuppressLint("CheckResult")
    fun getVideoData(
        videoId: String,
        memberId: String
    ) {
        repository.getVideoData(videoId, memberId)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onSuccess = {

            }, onError = {

            }).addDisposable(compositeDisposable)
    }

    @SuppressLint("CheckResult")
    fun getVideoStatistics() {
        repository.getVideoStatistics()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onSuccess = {

            }, onError = {

            }).addDisposable(compositeDisposable)
    }
}