package com.example.demoporject.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.demoporject.model.user.SearchUserResponse
import com.example.demoporject.model.user.UserResponse
import com.example.demoporject.repository.UserRepo.UserRepositoryContact
import com.example.demoporject.utils.addDisposable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import retrofit2.Response

class UserViewModel(private val userRepository: UserRepositoryContact) : BaseViewModel() {
    companion object {
        private const val NOT_FOUND = 404
        private const val INTERNET_SERVER_ERROR = 500
    }

    val userData: LiveData<UserDataStatus> get() = _userDataLiveData
    private val _userDataLiveData = MutableLiveData<UserDataStatus>()
    val userSearchData: LiveData<SearchUserResponse> get() = _userSearchData
    private val _userSearchData = MutableLiveData<SearchUserResponse>()

    fun addUserData(userID: String, userPwd: String, memberName: String,
        mail: String, quoData: String) {
        userRepository.addUserData(userID = userID, passWord = userPwd,
            memberName = memberName, mail = mail, quota = quoData)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onSuccess = {
                it.setLiveData(_userDataLiveData, UserDataStatus.AddStatus())
            }, onError = {
                Log.d("UserViewModel", "$it")
            }).addDisposable(compositeDisposable)
    }

    fun modifyUserData(userID: String, userPwd: String,
        memberName: String, mail: String, quoData: String) {
        userRepository.modifyUserData(
            userID = userID, passWord = userPwd, memberName = memberName,
            mail = mail, quota = quoData)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onSuccess = {
                it.setLiveData(_userDataLiveData, UserDataStatus.UpDateStatus())
            }, onError = {
                Log.d("UserViewModel", "$it")
            }).addDisposable(compositeDisposable)
    }

    fun searchUserData(userID: String) {
        userRepository.searchUserData(userID = userID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onSuccess = {
                it.body()?.let {
                    _userSearchData.value = it
                }
            }, onError = {
                Log.d("UserViewModel", "$it")
            }).addDisposable(compositeDisposable)
    }

    fun deleteUserData(userID: String) {
        userRepository.deleteUserData(userID = userID)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(onSuccess = {
            it.setLiveData(_userDataLiveData, UserDataStatus.DeleteStatus())
        }, onError = {
            Log.d("userDataViewModel", it.toString())
        }).addDisposable(compositeDisposable)
    }

    private fun Response<UserResponse>.setLiveData(
        liveData: MutableLiveData<UserDataStatus>,
        userDataStatus: UserDataStatus
    ) {
        if (isSuccessful) {
            body()?.let {
                val statusStr = it.message
                when (userDataStatus) {
                    is UserDataStatus.DeleteStatus -> liveData.value =
                        if (it.code == 0) UserDataStatus.DeleteStatus("成功") else UserDataStatus.DeleteStatus("失敗")
                    is UserDataStatus.UpDateStatus -> liveData.value =
                        if (it.code == 0) UserDataStatus.UpDateStatus("成功") else UserDataStatus.UpDateStatus("失敗")
                    is UserDataStatus.AddStatus -> liveData.value =
                        if (it.code == 0) UserDataStatus.AddStatus("成功") else UserDataStatus.AddStatus("失敗")
                    else -> UserDataStatus.NONE
                }
            }
        } else {
            when (code()) {
                NOT_FOUND -> _userDataLiveData.value =
                    UserDataStatus.DeleteStatus("${errorBody()}")
                INTERNET_SERVER_ERROR -> _userDataLiveData.value =
                    UserDataStatus.DeleteStatus("${errorBody()}")
            }
        }
    }
}

sealed class UserDataStatus {
    data class DeleteStatus(val data: String? = null) : UserDataStatus()
    data class UpDateStatus(val data: String? = null) : UserDataStatus()
    data class AddStatus(val data: String? = null) : UserDataStatus()
    object NONE : UserDataStatus()
}