package com.example.jhtestdemo.common.util

import android.util.Base64
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

object EncryptDecryptUtil {

    private const val password = "0123456789abcdef"
    private const val iv = "0123456789abcdef"

    /**
     * AES加密
     */
    fun encryptAES(encryptStr: String): String {
        try {
            val cipher = getCipher(Cipher.ENCRYPT_MODE)
            val encrypted = cipher.doFinal(encryptStr.toByteArray(charset("UTF-8")))
            val encode = Base64.encode(encrypted, Base64.NO_WRAP)
            return String(encode)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return ""
    }

    /**
     * AES解密
     */
    fun decryptAES(encryptedStr: String): String {
        try {
            val cipher = getCipher(Cipher.DECRYPT_MODE)
            val decode = Base64.decode(encryptedStr.toByteArray(charset("UTF-8")), Base64.NO_WRAP)
            val original = cipher.doFinal(decode)
            return String(original)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return ""
    }

    fun getCipher(mode: Int): Cipher {
        val password =
            SecretKeySpec(password.toByteArray(charset("UTF-8")), "AES")
        val iv = IvParameterSpec(iv.toByteArray(charset("UTF-8")))
        val cipher = Cipher.getInstance("AES/CBC/PKCS7PADDING")
        cipher.init(mode, password, iv)
        return cipher
    }

    /**
     *  MD5加密
     */
    fun encodeMD5(text: String): String {
        try {
            val instance: MessageDigest = MessageDigest.getInstance("MD5")
            val digest: ByteArray = instance.digest(text.toByteArray())
            val sb = StringBuffer()
            for (b in digest) {
                val i: Int = b.toInt() and 0xff
                var hexString = Integer.toHexString(i)
                if (hexString.length < 2) hexString = "0$hexString"
                sb.append(hexString)
            }
            return sb.toString()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
        return ""
    }
}