package com.example.demoporject.utils

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable


fun Disposable.addDisposable(compositeDisposable: CompositeDisposable) {
    compositeDisposable.add(this)
}
