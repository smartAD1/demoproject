package com.example.demoporject.utils

import android.util.Base64
import java.security.spec.AlgorithmParameterSpec
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

object CryptManager {
    val IvAES = "0123456789abcdef".toByteArray()
    val KeyAES = "0123456789abcdef".toByteArray()
    val mSecretKeySpec = SecretKeySpec(KeyAES, "AES")
    val mAlgorithmParameterSpec: AlgorithmParameterSpec = IvParameterSpec(IvAES)

    fun EncryptAES(text: String): String {
        try {
            val mCipher = getCipher()
            val crypeed = mCipher.doFinal(text.toByteArray())
            val encode = Base64.encode(crypeed,Base64.NO_WRAP)
            return String(encode)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return ""
    }

    private fun getCipher(): Cipher {
        val mCipher = Cipher.getInstance("AES/CBC/PKCS7PADDING")
        mCipher.init(Cipher.ENCRYPT_MODE, mSecretKeySpec, mAlgorithmParameterSpec)
        return mCipher
    }

    //AES解密，帶入byte[]型態的16位英數組合文字、32位英數組合Key、需解密文字
    fun DecryptAES(text: String): ByteArray? {
        return try {
            Decode(Base64.decode(text, Base64.DEFAULT))
        } catch (ex: java.lang.Exception) {
            null
        }
    }
    private fun Decode(text: ByteArray): ByteArray? {
        val mCipher = Cipher.getInstance("AES/CBC/PKCS7PADDING")
        mCipher.init(Cipher.DECRYPT_MODE, mSecretKeySpec, mAlgorithmParameterSpec)
       return mCipher.doFinal(text)
    }
}