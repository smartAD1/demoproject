package com.example.demoporject.apiUtils.Interceptors

import android.util.Log
import com.example.demoporject.utils.CryptManager
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import java.nio.charset.Charset

class DeCodeInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        var response = chain.proceed(request)
        if (response.isSuccessful) {
            val responseBody = response.body
            if (responseBody != null) {
                try {
                    val source = responseBody.source()
                    Log.d("more", "source: $source")
                    source.request(java.lang.Long.MAX_VALUE)
                    val buffer = source.buffer()
                    var charset = Charset.forName("UTF-8")
                    val contentType = responseBody.contentType()
                    if (contentType != null) charset = contentType.charset(charset)
                    val bodyString = buffer.clone().readString(charset)
                    val textByte: ByteArray? =
                        CryptManager.DecryptAES(
                            bodyString
                        )
                    val newBodyString = textByte?.let { String(it) }
                    val newResponseBody = ResponseBody.create(contentType, newBodyString ?: "")
                    response = response.newBuilder().body(newResponseBody).build()
                } catch (e: Exception) {
                    Log.d("more", "解密異常︰${e}")
                    return response
                }
            } else {
                Log.d("more", "responseBody == null")
            }
        }
        return response
    }
}

