package com.example.jhtestdemo.common.remote

import android.util.Log
import com.example.demoporject.utils.CryptManager
import com.example.demoporject.utils.ToMD5
import com.example.demoporject.utils.encodeMD5
import com.example.jhtestdemo.common.util.EncryptDecryptUtil
import okhttp3.Interceptor
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.Response
import okio.Buffer


class HeaderInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val requestBody: RequestBody = request.body!!
        val signStringBuilder = StringBuilder()
        val nameMutableList: MutableList<String> = mutableListOf()
        val mutableMap: MutableMap<String, String> = mutableMapOf()
        getMultipartBodyNameValue(requestBody, nameMutableList, mutableMap)
        val sign = getSign(nameMutableList, mutableMap, signStringBuilder)
        request = request.newBuilder().addHeader("sign", sign).build()
        return chain.proceed(request)
    }

    private fun getMultipartBodyNameValue(
        requestBody: RequestBody,
        nameMutableList: MutableList<String>,
        mutableMap: MutableMap<String, String>
    ) {
        if (requestBody is MultipartBody) {
            val buffer1 = Buffer()
            requestBody.writeTo(buffer1)
            val postParams = buffer1.readUtf8()
            val split = postParams.split("\n".toRegex()).toTypedArray()
            val names: MutableList<String> = ArrayList()
            for (s in split) {
                if (s.contains("Content-Disposition")) {
                    names.add(
                        s.replace("Content-Disposition: form-data; name=", "")
                            .replace("\"", "")
                    )
                }
            }
            val parts = requestBody.parts
            for (i in parts.indices) {
                val part = parts[i]
                val body1 = part.body
                if (body1.contentLength() < 100) {
                    val buffer = Buffer()
                    body1.writeTo(buffer)
                    val value = buffer.readUtf8()
                    if (names.size > i) {
                        nameMutableList.add(EncryptDecryptUtil.decryptAES(names[i]))
                        Log.d("more", "HeaderInterceptor, names: ${names[i]}")
                        Log.d("more", "HeaderInterceptor, value: $value")
                        // mutableMap[names[i]] = value
                        mutableMap[EncryptDecryptUtil.decryptAES(names[i])] =
                            EncryptDecryptUtil.decryptAES(value)
                    }
                }
            }
        }
    }

    private fun getSign(
        nameMutableList: MutableList<String>,
        mutableMap: MutableMap<String, String>,
        signStringBuilder: StringBuilder
    ): String {
        val nameSortedList = nameMutableList.sorted()
        for (i in nameSortedList.indices) {
            val name = nameSortedList[i]
            val value = mutableMap[name]
            if (i < nameSortedList.size - 1) {
                signStringBuilder.append("$name=$value&")
            } else {
                signStringBuilder.append("$name=$value")
            }
        }
        Log.d("more", "signStringBuilder.toString(): ${signStringBuilder.toString()}")
        return EncryptDecryptUtil.encodeMD5(signStringBuilder.toString())
    }
}