package com.example.demoporject.apiUtils

import com.example.demoporject.model.Video.GetVideoListResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface VideoApiService {

    @Multipart
    @POST("api/GetVideoList.php")
    fun getVideoList(
        @Part("pageNo") pageNo: String,
        @Part("pageSize") pageSize: String
    ): Single<Response<GetVideoListResponse>>

    @Multipart
    @POST("api/GetVideoData.php")
    fun getVideoData(
        @Part("videoId") videoId: String,
        @Part("member_id") memberId: String
    ): Single<Response<GetVideoListResponse>>

    @Multipart
    @POST("api/GetVideoStatistics.php")
    fun getVideoStatistics(
        @Part("type") type: String
    ): Single<Response<GetVideoListResponse>>
}