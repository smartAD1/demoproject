package com.example.demoporject.apiUtils.Interceptors
import com.example.jhtestdemo.common.util.EncryptDecryptUtil
import okhttp3.Interceptor
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.Response
import okio.Buffer

class PartInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val requestBody: RequestBody = request.body!!
        val nameMutableList: MutableList<String> = mutableListOf()
        val valueMutableList: MutableList<String> = mutableListOf()
        getMultipartBodyEncryptNameValue(requestBody, nameMutableList, valueMutableList)
        val multipartBodyBuilder = MultipartBody.Builder().setType(MultipartBody.FORM)
        for (i in nameMutableList.indices) {
            multipartBodyBuilder.addFormDataPart(nameMutableList[i], valueMutableList[i])
        }
        val requestMultipartBody: RequestBody = multipartBodyBuilder.build()
        request = request.newBuilder().post(requestMultipartBody).build()
        return chain.proceed(request)
    }

    private fun getMultipartBodyEncryptNameValue(requestBody: RequestBody?, nameMutableList: MutableList<String>,
            valueMutableList: MutableList<String>) {
        if (requestBody is MultipartBody) {
            val buffer1 = Buffer()
            requestBody.writeTo(buffer1)
            val postParams = buffer1.readUtf8()
            val split = postParams.split("\n".toRegex()).toTypedArray()
            val names: MutableList<String> = ArrayList()
            for (s in split) {
                if (s.contains("Content-Disposition")) {
                    names.add(
                        s.replace("Content-Disposition: form-data; name=", "")
                            .replace("\"", "")
                    )
                }
            }
            val parts = requestBody.parts
            for (i in parts.indices) {
                val part = parts[i]
                val body1 = part.body
                if (body1.contentLength() < 100) {
                    val buffer = Buffer()
                    body1.writeTo(buffer)
                    val value = buffer.readUtf8()
                    if (names.size > i) {
                        val cryptName = EncryptDecryptUtil.encryptAES(names[i])
                        val cryptValue = EncryptDecryptUtil.encryptAES(value)
//                        nameMutableList.add(cryptName)
//                        valueMutableList.add(cryptValue)
                        nameMutableList.add(EncryptDecryptUtil.encryptAES(names[i].getDefaultEncoding()))
                        valueMutableList.add(EncryptDecryptUtil.encryptAES(value))
                    }
                }
            }
        }
    }

    private fun String.getDefaultEncoding(): String {
        when {
            this.contains("userid") -> return "userid"
            this.contains("password") -> return "password"
            this.contains("member_name") -> return "member_name"
            this.contains("mail") -> return "mail"
            this.contains("quota") -> return "quota"
            this.contains("pageNo") -> return "pageNo"
            this.contains("pageSize") -> return "pageSize"
            this.contains("videoId") -> return "videoId"
            this.contains("member_id") -> return "member_id"
            this.contains("type") -> return "type"
        }
        return ""
    }
}

