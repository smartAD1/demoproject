package com.example.demoporject.apiUtils


import com.example.demoporject.apiUtils.Interceptors.PartInterceptor
import com.example.demoporject.apiUtils.Interceptors.DeCodeInterceptor
import com.example.jhtestdemo.common.remote.HeaderInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory


object RetrofitManager {

    val okhttpLog = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    val url = "http://47.242.40.2/demo/"

    private val okHttpBuilder = OkHttpClient.Builder()
        .addInterceptor(PartInterceptor())
        .addInterceptor(HeaderInterceptor())
        .addInterceptor(DeCodeInterceptor())
        .addInterceptor(okhttpLog)
        .build()

    fun getUserRetrofitManager(): UserApiService =
        Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpBuilder)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(UserApiService::class.java)

    fun getVideoRetrofitManager(): VideoApiService =
        Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpBuilder)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(VideoApiService::class.java)
}

