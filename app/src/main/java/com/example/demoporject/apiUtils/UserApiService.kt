package com.example.demoporject.apiUtils

import com.example.demoporject.model.user.SearchUserResponse
import com.example.demoporject.model.user.UserResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface UserApiService {
    @Multipart
    @POST("api/CreateUser.php")
    fun addUserData(@Part("userid") userId: String,
                     @Part("password") password: String,
                     @Part("member_name") memberName: String,
                     @Part("mail") mail: String,
                     @Part("quota") quota: String): Single<Response<UserResponse>>

    @Multipart
    @POST("api/UpdateUser.php")
    fun modifyUserData(@Part("userid") userid: String, @Part("password") password: String,
                       @Part("member_name") member_name: String,
                       @Part("mail") mail: String,
                       @Part("quota") quota: String): Single<Response<UserResponse>>

    @Multipart
    @POST("api/DeleteUser.php")
    fun deleteUserData(@Part("userid") userid: String): Single<Response<UserResponse>>

    @Multipart
    @POST("api/SearchUser.php")
    fun searchUserData(@Part("userid") userid: String): Single<Response<SearchUserResponse>>


}
